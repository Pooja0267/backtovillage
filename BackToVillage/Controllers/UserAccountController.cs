﻿/***********************************************************************************
 *                                                                                 *
 *    File Name:   UserAccountController                                           *
 *                                                                                 *
 *    Created On:  08/08/2016                                                      *
 *                                                                                 *
 *    Description: This file defines the methods that perform the basic            *
 *                 required functionalities related to an user's account like      *                 
 *                 Register, Log In, Edit Profile, Change Password and             * 
 *                 Delete Account.                                                 *
 *                                                                                 *
 ***********************************************************************************/

using BackToVillage.BusinessLayer;
using BackToVillage.Filters;
using BackToVillage.Utilities;
using BackToVillage.ViewModels;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BackToVillage.Controllers
{
    public class UserAccountController : Controller
    {
        AccountBl userAccount = new AccountBl();

        // GET: UserProfile
        public ActionResult Register()
        {
            var user = userAccount.GetLists();
            return View(user);
        }

        /// <summary>
        /// Registers Users
        /// </summary>
        /// <param name="userObject"></param>
        /// <returns> Register View</returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(UserDetails userObject)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!userAccount.CheckEmail(userObject.Email))
                    {
                       bool status =  userAccount.AddUser(userObject);
                       ViewBag.Message = status? BTV_Constants.RegistrationSuccess: BTV_Constants.RegistrationFailure;
                       
                    }
                    else
                    {
                        ViewBag.Message = BTV_Constants.EmailExists;
                    }

                    ModelState.Clear();
                }
                
            }
            catch (Exception ex)
            {
                // If we got this far, something failed, redisplay form
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                ModelState.AddModelError("Error", BTV_Constants.ModelError);
            }
            
            var lists = userAccount.GetLists();
            return View(lists);
        }

        //GET: Login
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// User Login 
        /// </summary>
        /// <param name="user"></param>
        /// <returns>Profile View Based on Role</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginVm user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string password = user.Password;
                    user.HashPassword = password.GetHashCode().ToString();
                    var result = userAccount.UserLogin(user);

                    if (result)
                    {
                        var userId = new AccountBl().GetUserId(user.Email);
                        var roleId = userAccount.GetRole(userId);
                        Session["user"] = userId;

                        switch (roleId)
                        {
                            case (int)UserRoles.Admin:
                                return RedirectToAction("AdminProfile", "Admin");

                            case (int)UserRoles.GroupMember:
                                return RedirectToAction("UserProfile", "User");

                            case (int)UserRoles.User:
                                return RedirectToAction("UserProfile", "User");

                            default:
                                return View();
                        }
                    }
                    ViewBag.Message = BTV_Constants.LoginFail;
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                ModelState.AddModelError("Error", BTV_Constants.ModelError);
            }
            return View();
        }

        /// <summary>
        /// Bind Cities according to State selection
        /// </summary>
        /// <param name="stateId"></param>
        /// <returns>City List according to State selection</returns>
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadCitiesByState(int stateId)
        {
            try
            {
                if (stateId > 0)
                {
                    var city = new AccountBl();

                    var cityList = city.GetCities(Convert.ToInt32(stateId));
                    var cityData = cityList.Select(m => new SelectListItem()
                    {
                        Text = m.CityName,
                        Value = m.CityId.ToString(),
                    });
                    return Json(cityData, JsonRequestBehavior.AllowGet);
                }

                ModelState.AddModelError("Error", BTV_Constants.ModelError);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                ModelState.AddModelError("Error", BTV_Constants.ModelError);
            }
            return null;
        }

        [AuthenticateUser]
        public ActionResult LogOut()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Index", "Home");

        }

        /// <summary>
        /// To perform Photo Upload
        /// </summary>
        /// <param name="imageData"></param>
        /// <returns></returns>
        [HttpPost]
        public string UploadPhoto(HttpPostedFileBase imageData)
        {
            try
            {
                var photoPath = ConfigurationManager.AppSettings["ProfilePhotoPath"];

                if (imageData != null && imageData.ContentLength > 0)
                {
                    AccountBl user = new AccountBl();

                    // extract the fielname
                    var fileName = Path.GetFileName(imageData.FileName);

                    if (!String.IsNullOrEmpty(fileName))
                    {
                        // store the file in a separate folder
                        var path = Path.Combine(photoPath, fileName);
                        var uid = (int)Session["user"];
                        imageData.SaveAs(path);
                        user.SavePhotoPath(path, uid);
                        string profilePhotoPath = userAccount.GetProfilePhoto(path);
                        ViewBag.ImageUrl = profilePhotoPath;
                        ViewBag.ImageUrl = userAccount.GetProfilePhoto(path);
                        return profilePhotoPath;
                    }

                }
            }

            catch (Exception ex)
            {
                ViewBag.Message = BTV_Constants.PhotoUploadError;
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return String.Empty;

        }

        /// <summary>
        /// Edit Profile
        /// </summary>
        [AcceptVerbs(HttpVerbs.Get)]
        [AuthenticateUser]
        public ActionResult Edit()
        {
            var userId = (int)Session["user"];
            EditProfile userDetails = userAccount.GetuserDetailsAndLists(userId);
           // if (userDetails.roleId == (int) UserRoles.Admin)
            //{
                return View(userDetails);
            //}

           // return PartialView("_EditProfile",userDetails);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditProfile user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!userAccount.CheckEmail(user.Email))
                    {
                        user.UserId = (int)Session["user"];
                        userAccount.EditUser(user);
                        ViewBag.Message = BTV_Constants.EditProfileSuccess;
                    }

                    else
                    {
                        ViewBag.Message = BTV_Constants.EmailExists;
                    }
                    ModelState.Clear();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                ModelState.AddModelError("Error", BTV_Constants.ModelError);
            }

            var userId = (int)Session["user"];
            EditProfile userDetails = userAccount.GetuserDetailsAndLists(userId);
            return View(userDetails);
        }

        /// <summary>
        /// For User to Change Password
        /// </summary>
        [AuthenticateUser]
        public ActionResult ChangePassword()
        {
            EditProfile user = new EditProfile();

            var userId = (int)Session["user"];
            user.roleId = userAccount.GetRole(userId);
           // if (user.roleId == (int)UserRoles.Admin)
            //{
                return View(user);
            //}

            //return PartialView("_ChangePassword", user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(EditProfile user)
        {
            EditProfile userEditAccount = new EditProfile();
            try
            {
                if (!String.IsNullOrEmpty(user.Password))
                {
                    var userId = (int)Session["user"];
                    userAccount.ChangePassword(userId, user.Password);
                    ViewBag.Message = BTV_Constants.PasswordChangeSuccess;
                    ModelState.Clear();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                ModelState.AddModelError("Error", BTV_Constants.ModelError);
            }

            var roleId = (int)Session["user"];
            user.roleId = userAccount.GetRole(roleId);
            return View(userEditAccount);
        }

        /// <summary>
        /// Delete account
        /// </summary>
        [AuthenticateUser]
        public ActionResult Delete()
        {
            var userId = (int)Session["user"];
            userAccount.DeleteUser(userId);
            ViewBag.Message = BTV_Constants.AccountDeleted;
            return RedirectToAction("LogOut", "UserAccount");
        }
    }
}




