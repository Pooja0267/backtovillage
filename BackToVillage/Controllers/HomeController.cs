﻿/***********************************************************************************
 *                                                                                 *
 *    File Name:   HomeController                                                  *
 *                                                                                 *
 *    Created On:  10/08/2016                                                      *
 *                                                                                 *
 *    Description: This file defines the basic Action methods that return views    *
 *                 that are accessible to any Anonymous Users.                     *
 *                                                                                 *
 *                                                                                 *
 ***********************************************************************************/


using BackToVillage.BusinessLayer;
using System.Web.Mvc;

namespace BackToVillage.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Error()
        {
            return View();
        }

        public ActionResult SuccessStories()
        {
            UserBl successStories = new UserBl();
           var stories = successStories.GetSuccessStories();
           return View(stories);
        }

       public ActionResult Gallery()
       {
           return View();
       }

       public ActionResult Error404()
       {
           return View("ErrorPage_404");
       }

       public ActionResult Error500()
       {
           return View("ErrorPage_500");
       }
    }
}