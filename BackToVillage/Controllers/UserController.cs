﻿/***********************************************************************************
 *                                                                                 *
 *    File Name:   UserController                                                  *
 *                                                                                 *
 *    Created On:  10/08/2016                                                      *
 *                                                                                 *
 *    Description: This file defines the methods that can be performed only by     *
 *                 an authorized User depending on their Role. It implements the   *
 *                 functionalities like Adding Work, Viewing, Updating, Removing   *
 *                 Added Work and Publising Success Story by a Group Member.       *                 
 *                 The registered users can report a Problem.                      *
 *                                                                                 *                         
 ***********************************************************************************/

using BackToVillage.BusinessLayer;
using BackToVillage.Filters;
using BackToVillage.Utilities;
using BackToVillage.ViewModels;
using System;
using System.Web.ModelBinding;
using System.Web.Mvc;
using System.Web.Services.Description;

namespace BackToVillage.Controllers
{
    [AuthenticateUser]
    public class UserController : Controller
    {
        UserBl userWorkObj = new UserBl();
        /// <summary>
        /// Gets User Specific Data and Displays Profile View
        /// </summary>
        /// <returns></returns>
        public ActionResult UserProfile()
        {
            var userDetails = new UserDetails();

            try
            {
                AccountBl userAccount = new AccountBl();
                var userId = (int)Session["user"];
                userDetails = userAccount.GetUserDetails(userId);

                if (!String.IsNullOrEmpty(userDetails.ProfilePhotoPath))
                {
                    ViewBag.ImageUrl = userAccount.GetProfilePhoto(userDetails.ProfilePhotoPath);
                }

                else
                {
                    ViewBag.ImageUrl = userDetails.Gender == "Male"
                       ? userAccount.GetProfilePhoto(BTV_Constants.DefaultMaleProfileImage)
                       : userAccount.GetProfilePhoto(BTV_Constants.DefaultFemaleProfileImage);
                }
                Session["userName"] = userDetails.Username;
                Session["profileImage"] = ViewBag.ImageUrl;
                return View(userDetails);
                //return PartialView("_UserProfile", userDetails);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                ModelState.AddModelError("Error", BTV_Constants.ModelError);
            }
            return View(userDetails);
        }

        /// <summary>
        /// A Group member can add a Work in a particular area and request the Admin to Approve it.
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        [AuthorizeUserRole("GroupMember")]
        public ActionResult AddWork()
        {
            var userId = (int)Session["user"];
            ViewBag.UserId = userId;
            var lists = userWorkObj.GetLists();
            return View(lists);
            //return PartialView("_AddWork", lists);
        }

        
        [HttpPost]
        public ActionResult AddWork(WorkDetailsVm work)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    work.UserId = (int)Session["user"];
                    bool status = userWorkObj.AddWork(work);
                    ViewBag.Message = status ? BTV_Constants.WorkAddedSuccess : BTV_Constants.Error;
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            ModelState.Clear();
            var lists = userWorkObj.GetLists();
            return View(lists);
        }

        /// <summary>
        /// A Group member can view all the work added by him/her and whether it has been approved or not.
        /// </summary>
        /// <returns></returns>
        /// 
        [AcceptVerbs(HttpVerbs.Get)]
        [AuthorizeUserRole("GroupMember")]
        public ActionResult ViewWorks()
        {
            var works = userWorkObj.GetWorksAdded((int)Session["user"]);

            if (works == null)
            {
                ViewBag.Message = BTV_Constants.NoWorkAdded;
            }

            return View(works);
            //return PartialView("_Test", works);
           // return PartialView("_ViewWorks", works);
        }

        /// <summary>
        /// A Group Member can remove any of his work if it is rejected 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [AuthorizeUserRole("GroupMember")]
        public ActionResult RemoveWork(int userId)
        {
            userWorkObj.RemoveWork(userId);
            ViewBag.Message = BTV_Constants.WorkRemoved;
            return RedirectToAction("ViewWorks");
        }


        /// <summary>
        /// A Group Member can update any of his work that has been Initialized, Started or is InProgress
        /// </summary>
        /// <param name="workId"></param>
        /// <returns></returns>
        [AcceptVerbs(HttpVerbs.Get)]
        [AuthorizeUserRole("GroupMember")]
        public ActionResult UpdateWork(int workId)
        {
            ViewBag.workid = workId;
            var workDetails = userWorkObj.GetWorkStatus(workId);
            //return View(workDetails);
            return PartialView("_UpdateWork", workDetails);
        }

        [HttpPost]
        public string UpdateWork(WorkDetailsVm work)
        {
            try
            {
                bool status = userWorkObj.UpdateWork(work);
                ViewBag.Message = status ? BTV_Constants.WorkUpdateSuccess : BTV_Constants.WorkUpdateFailure;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            
            //var works = userWorkObj.GetWorksAdded((int)Session["user"]);
            //return RedirectToAction("ViewWorks", works);
            //var workDetails = userWorkObj.GetWorkStatus(work.WorkId);
            //return PartialView("_UpdateWork", workDetails);
            return ViewBag.Message;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult FilterByWorkStatus(int selectedWorkStatusId)
        {
           var gridData =  userWorkObj.GetWorksByWorkStatus((int)Session["user"], selectedWorkStatusId );
           return PartialView("_WorkGrid", gridData.WorksList);
        }

        [AuthorizeUserRole("GroupMember")]
        public ActionResult PublishSuccessStory(int workId)
        {
            var work = userWorkObj.WorkDetailsForSuccessStory(workId);
            return View(work);
        }

        [HttpPost]
        public ActionResult PublishSuccessStory(WorkDetailsVm successStory)
        {
            try
            {
                successStory.UserId = (int)Session["user"];
                bool status = userWorkObj.AddSuccessStory(successStory);
                ViewBag.Message = status ? BTV_Constants.StorySuccess : BTV_Constants.StoryFailure;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
           
            return RedirectToAction("ViewWorks");
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [AuthorizeUserRole("User")]
        public ActionResult ReportAnIssue()
        {
            var lists = userWorkObj.GetListsForProblems();
            //return PartialView("_ReportAnIssue",lists);
            return View(userWorkObj.GetListsForProblems());
        }

        [HttpPost]
        public ActionResult ReportAnIssue(ProblemIssuesVm problemObj)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    UserBl userObject = new UserBl();
                    problemObj.UserId = (int)Session["user"];
                    bool status = userObject.AddProblem(problemObj);
                    ViewBag.Message = status ? BTV_Constants.ProblemReportSuccess : ViewBag.Message = BTV_Constants.ProblemReportFail;
                    ModelState.Clear();
                    
                }
                else
                {
                    ViewBag.Message = BTV_Constants.Error;
                }

            }
            catch(Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
                        
            return View(userWorkObj.GetListsForProblems());            
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RefreshWorks()
        {
            var gridData = userWorkObj.GetWorksAdded((int)Session["user"]);
            return PartialView("_WorkGrid", gridData.WorksList);
        } 
       
        public ActionResult UnAuthorizedAccess()
        {
            return View();
           // return PartialView("_UnAuthorizedAccess");
        }


        //[AuthorizeUserRole("GroupMember")]
        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult DashBoard()
        //{
        //TODO
        //}


        //[AuthorizeUserRole("GroupMember")]
        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult AjaxTrial()
        //{
          
        //    return PartialView("",);
        //}
    }
}


