﻿/***********************************************************************************
 *                                                                                 *
 *    File Name:   AdminController                                                 *
 *                                                                                 *
 *    Created On:  10/08/2016                                                      *
 *                                                                                 *
 *    Description: This file defines the methods that can be performed only by     *
 *                 an authorized Admin. It implements the functionalities like     *
 *                 Approving and Rejecting Group Member Requests and Work Requests.*                 
 *                 Viewing All Registered users, All Works, Reported Issues and    *
 *                 Assigning work to Group Member based on Reported problems.      * 
 *                                                                                 *
 *                                                                                 *
 ***********************************************************************************/

using BackToVillage.BusinessLayer;
using BackToVillage.Filters;
using BackToVillage.Utilities;
using BackToVillage.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;

namespace BackToVillage.Controllers
{
    [AuthenticateUser]
    [AuthorizeUserRole("Admin")]
    public class AdminController : Controller
    {

        AdminBl adminObj = new AdminBl();

        // GET: Admin
        public ActionResult AdminProfile()
        {
            var userDetails = new UserDetails();

            try
            {
                var userId = (int)Session["user"];
                AccountBl user = new AccountBl();
                userDetails = user.GetUserDetails(userId);

                if (!String.IsNullOrEmpty(userDetails.ProfilePhotoPath))
                {
                    ViewBag.ImageUrl = user.GetProfilePhoto(userDetails.ProfilePhotoPath);
                }

                else
                {
                    ViewBag.ImageUrl = userDetails.Gender == "Male"
                        ? user.GetProfilePhoto(BTV_Constants.DefaultMaleProfileImage)
                        : user.GetProfilePhoto(BTV_Constants.DefaultFemaleProfileImage);
                }
                
                Session["userName"] = userDetails.Username;
                Session["profileImage"] = ViewBag.ImageUrl;
                return View(userDetails);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                ModelState.AddModelError("Error", BTV_Constants.ModelError);
            }
            return View(userDetails);
        }

        // Get List of Users who want to Volunteer as Group Members
        public ActionResult GroupMemberRequests()
        {
            return View(adminObj.GetGroupMemberRequestsList());
        }

        // Approve Group Member Requests
        public ActionResult ApproveRequest(string emailId)
        {
            adminObj.ApproveGroupMember(emailId);
            return View("GroupMemberRequests", adminObj.GetGroupMemberRequestsList());
        }

        //Reject Group Member Requests
        public ActionResult RejectRequest(string emailId)
        {
            adminObj.RejectGroupMember(emailId);
            return View("GroupMemberRequests", adminObj.GetGroupMemberRequestsList());
        }

        // Get List of All Users
        public ActionResult GetAllUsers()
        {
            return View(adminObj.GetAllUsers());
        }

        // Get Grid View of Work Requests
        public ActionResult WorkRequests()
        {
            return View(adminObj.workRequests());
        }

        // Approve Group Member Requests
        public ActionResult ApproveWorkRequest(int workId)
        {
            ViewBag.workId = workId;
            var workObj = new WorkDetailsVm();
            return View(workObj);
        }

        [HttpPost]
        public ActionResult ApproveWorkRequest(WorkDetailsVm work)
        {
            adminObj.ApproveWorkRequest(work);
            ViewBag.Message = BTV_Constants.WorkApproved;
            return RedirectToAction("WorkRequests");
        }

        //Reject Group Member Requests
        public ActionResult RejectWorkRequest(int workId)
        {
            adminObj.RejectWorkRequest(workId);
            ViewBag.Message = BTV_Constants.WorkRejected;
            return View(adminObj.workRequests());
        }

        //View All Work
        public ActionResult ViewAllWork()
        {
            return View(adminObj.ViewAllWork());
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult FilterByWorkStatus(int selectedWorkStatusId)
        {
            //int? i = selectedWorkStatusId ?? 0;
            var gridData = adminObj.GetWorksByWorkStatus(selectedWorkStatusId);
            return PartialView("_AllWork", gridData.WorksList);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult FilterByDepartment(int selectedDomainId)
        {
            var gridData = adminObj.GetWorksByDepartment(selectedDomainId);
            return PartialView("_AllWork", gridData.WorksList);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ReportedProblems()
        {
            return View(adminObj.ViewAllReportedProblems());
        }

        public ActionResult AssignWork(int problemId)
        {
            var workDetails = new WorkDetailsVm();

            workDetails.ProblemId = problemId;
            workDetails = adminObj.GetAdminWorkDetails();
            return View(workDetails);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadGroupMembersByCity(string cityId)
        {
            try
            {
                if (String.IsNullOrEmpty(cityId) != true)
                {
                    var groupMemberList = adminObj.GetGroupMembers(Convert.ToInt32(cityId));
                    var groupMemberData = groupMemberList.Select(m => new SelectListItem()
                    {
                        Text = m.UserName,
                        Value = m.UserId.ToString(),
                    });
                    return Json(groupMemberData, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return null;
        }

        [HttpPost]
        public ActionResult AssignWork(WorkDetailsVm assignedWork)
        {
            try
            {
                var status = adminObj.AssignWorkDetails(assignedWork);

                if (status)
                {
                    ViewBag.Message = BTV_Constants.WorkAssigned;
                    ViewBag.WorkStatus = 1;
                }

                else
                {
                    ViewBag.Message = BTV_Constants.Error; 
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SearchByUserName(string userName)
        {
            var gridData = adminObj.SearchByUserName(userName);
            return PartialView("_AllUsers", gridData.UsersList);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RefreshAllUsers()
        {
            var gridData = adminObj.GetAllUsers();
            return PartialView("_AllUsers", gridData.UsersList);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RefreshAllWorks()
        {
            var gridData = adminObj.ViewAllWork();
            return PartialView("_AllWork", gridData.WorksList);
        }
    }
}


