﻿using BackToVillage.DataAccessLayer;
using BackToVillage.Utilities;
using BackToVillage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;


namespace BackToVillage.BusinessLayer
{
    public class UserBl
    {

        UserDal userRepo = new UserDal();
        WorkDetailsVm workObj = new WorkDetailsVm();
        ListsDal listRepo = new ListsDal();

        public bool AddWork(WorkDetailsVm work)
        {
            bool status = userRepo.AddWork(work);
            return status;
        }

        public WorkDetailsVm GetWorksAdded(int userId)
        {
            var userWorkListObj = new WorkDetailsVm();
            userWorkListObj.WorksList = userRepo.GetWorkList(userId).ToList();
            userWorkListObj.WorkStatusList = listRepo.FillWorkStatus();
            return userWorkListObj;
        }

        public WorkDetailsVm GetLists()
        {
            workObj.CityList = listRepo.FillCity();
            workObj.StateList = listRepo.FillState();
            workObj.DepartmentsList = listRepo.FillDepartments();
            workObj.WorkStatusList = listRepo.FillWorkStatus();

            return workObj;

        }

        public void RemoveWork(int userId)
        {
            userRepo.RemoveWork(userId);
        }

        public WorkDetailsVm GetWorkStatus(int workId)
        {
            workObj = userRepo.GetWorkStatus(workId);
            workObj.WorkStatusList = listRepo.FillWorkStatus();
            workObj.WorkStatusList.RemoveAll(i => (i.WorkStatusId == (int)WorkStatus.Initialised) || (i.WorkStatusId == (int)WorkStatus.Pending) || (i.WorkStatusId == (int)WorkStatus.Rejected));
            var defaultDate = new DateTime(0001, 01, 01);
            var startingDate = DateTime.Compare(workObj.StartingDate, defaultDate);
            if (startingDate == 0)
            {
                workObj.StartingDate = new DateTime(1950, 01, 01);

            }

            var endingDate = DateTime.Compare(workObj.EndingDate, defaultDate);
            if (endingDate == 0)
            {
                workObj.EndingDate = new DateTime(1950, 01, 01);
            }
            return workObj;

        }

        public bool UpdateWork(WorkDetailsVm work)
        {
            if (work.ProgressId == (int)WorkStatus.Closed)
            {
                work.EndingDate = DateTime.Today;
            }

            var defaultDate = new DateTime(0001, 01, 01);
            var startingDate = DateTime.Compare(work.StartingDate, defaultDate);
            if (startingDate == 0)
            {
                work.StartingDate = new DateTime(1950, 01, 01);

            }

            var endingDate = DateTime.Compare(work.EndingDate, defaultDate);
            if (endingDate == 0)
            {
                work.EndingDate = new DateTime(1950, 01, 01);
            }

            bool status = userRepo.UpdateWork(work);
            
            return status;
        }

        public WorkDetailsVm GetWorksByWorkStatus(int userId, int workStatusId)
        {
            var userWorkListObj = new WorkDetailsVm();
            userWorkListObj.WorksList = userRepo.GetWorkList(userId).ToList();
            userWorkListObj.WorksList.RemoveAll(i => i.ProgressId != workStatusId);
            return userWorkListObj;
        }

        public bool AddSuccessStory(WorkDetailsVm workStory)
        {
            bool status = userRepo.AddSuccessStory(workStory);
            return status;
        }

        public WorkDetailsVm WorkDetailsForSuccessStory(int workId)
        {
            var workDetails = userRepo.WorkDetails(workId);
            return workDetails;
        }

        public ProblemIssuesVm GetListsForProblems()
        {
            var problemObj = new ProblemIssuesVm();
            problemObj.CityList = listRepo.FillCity();
            problemObj.StateList = listRepo.FillState();
            problemObj.DepartmentsList = listRepo.FillDepartments();

            return problemObj;
        }

        public bool AddProblem(ProblemIssuesVm problemObj)
        {
            problemObj.ReportedOn = DateTime.Today;
            bool status = userRepo.AddProblem(problemObj);
            return status;
        }

        public List<WorkDetailsVm> GetSuccessStories()
        {
            var successStories = listRepo.GetSuccessStories();
            return successStories;
        }

    }
}
