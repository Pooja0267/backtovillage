﻿using BackToVillage.DataAccessLayer;
using BackToVillage.Models;
using BackToVillage.Utilities;
using BackToVillage.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace BackToVillage.BusinessLayer
{
    public class AccountBl
    {
        ListsDal listRepo = new ListsDal();
        AccountDal userRepo = new AccountDal();

        public UserDetails GetLists()
        {
            UserDetails userDetails = new UserDetails();

            userDetails.CityList = listRepo.FillCity();
            userDetails.StateList = listRepo.FillState();
            userDetails.OccupationList = listRepo.FillOccupation();
            return userDetails;
        }

        public bool CheckEmail(string email)
        {
            return (userRepo.CheckEmail(email));
        }

        public bool AddUser(UserDetails userObject)
        {
            string password = userObject.Password;
            userObject.DOB = userObject.DOB.Date;
            userObject.HashPassword = password.GetHashCode().ToString();
            bool status =  userRepo.AddUser(userObject);
            return status;
        }

        public List<City> GetCities(int stateId)
        {
            List<City> cityList = listRepo.BindCity(stateId);
            return cityList;
        }

        public bool UserLogin(LoginVm userCredentials)
        {
            var result = userRepo.MatchUserCredentials(userCredentials);
            return result.Equals(1);
        }

        public int GetRole(int userid)
        {
            var roleId = userRepo.GetRoleId(userid);
            return roleId;
        }

        public bool IsUserValid(int userId, string[] roles)
        {
            var roleName = String.Empty;
            switch (userId)
            {
                case (int)UserRoles.Admin:
                    roleName = "Admin";
                    break;
                case (int)UserRoles.GroupMember:
                    roleName = "GroupMember";
                    break;
                case (int)UserRoles.User:
                    roleName = "User";
                    break;
            }

            foreach (var role in roles)
            {
                return roleName == role;
            }

            return false;
        }

        public void SavePhotoPath(string path, int userId)
        {
            userRepo.SavePath(path, userId);
        }

        public int GetUserId(string email)
        {
            return (userRepo.GetUserId(email));
        }

        public UserDetails GetUserDetails(int userId)
        {
            var userDetails = userRepo.GetUserDetails(userId);
            userDetails.Username = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(userDetails.Username);
            return (userDetails);
        }

        public void DeleteUser(int userId)
        {
            userRepo.deleteUser(userId);
        }

        public void ChangePassword(int userId, string password)
        {
            string hashPassword = password.GetHashCode().ToString();
            userRepo.ChangePassword(userId, hashPassword);
        }


        public void EditUser(EditProfile userObject)
        {
            userRepo.EditUser(userObject);
        }

        // Gets the Lists required by the DropDown Lists 
        public EditProfile GetListsForEdit()
        {
            EditProfile objUserDetails = new EditProfile();

            objUserDetails.StateList = listRepo.FillState();
            objUserDetails.OccupationList = listRepo.FillOccupation();

            return objUserDetails;

        }

        // Returns the Details of a User and other Lists
        public EditProfile GetuserDetailsAndLists(int userId)
        {
            EditProfile objUserDetails = new EditProfile();

            objUserDetails = userRepo.GetEditUserDetails(userId);
            objUserDetails.StateList = listRepo.FillState();
            objUserDetails.OccupationList = listRepo.FillOccupation();
            objUserDetails.DOB = objUserDetails.DOB.Date;
            objUserDetails.SelectedCityId = objUserDetails.CityId;
            objUserDetails.SelectedStateId = objUserDetails.StateId;
            objUserDetails.SelectedOccupationId = objUserDetails.OccupationId;

            return objUserDetails;
        }

        // Returns the Base64String encoded path
        public string GetProfilePhoto(string path)
        {
            byte[] imageByteData = System.IO.File.ReadAllBytes(path);
            string imageBase64Data = Convert.ToBase64String(imageByteData);
            string imageDataUrl = string.Format("data:image/png;base64,{0}", imageBase64Data);
            return imageDataUrl;
        }
    }
}
