﻿using BackToVillage.DataAccessLayer;
using BackToVillage.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BackToVillage.BusinessLayer
{
    public class AdminBl
    {
        AdminDal adminObj = new AdminDal();
        ListsDal listRepo = new ListsDal();

        public List<UserDetails> GetGroupMemberRequestsList()
        {
            return (adminObj.GetGroupMemberRequests().ToList());
        }

        public void ApproveGroupMember(string id)
        {
            adminObj.ApproveRole(id);

        }
        public void RejectGroupMember(string id)
        {
            adminObj.RejectRole(id);
        }

        public List<WorkDetailsVm> workRequests()
        {
            return (adminObj.GetWorkRequests().ToList());
        }

        public void ApproveWorkRequest(WorkDetailsVm work)
        {
            adminObj.ApproveWorkRequest(work);

        }
        public void RejectWorkRequest(int userId)
        {
            adminObj.RejectWorkRequest(userId);
        }

        public UserDetails GetAllUsers()
        {
            var userObj = new UserDetails();
            userObj.UsersList = adminObj.GetAllUsers();
            return userObj;
        }

        public WorkDetailsVm ViewAllWork()
        {
            var allWorkListObj = new WorkDetailsVm();
            allWorkListObj.WorkStatusList = listRepo.FillWorkStatus();
            allWorkListObj.DepartmentsList = listRepo.FillDepartments();
            allWorkListObj.WorksList = adminObj.GetAllWork();

            return allWorkListObj;
        }

        public WorkDetailsVm GetWorksByWorkStatus(int workStatusId)
        {
            var userWorkListObj = new WorkDetailsVm();
            userWorkListObj.WorksList = adminObj.GetAllWork();
            userWorkListObj.WorksList.RemoveAll(i => i.ProgressId != workStatusId);
            return userWorkListObj;
        }

        public ProblemIssuesVm ViewAllReportedProblems()
        {
            var problemObj = new ProblemIssuesVm();
            problemObj.ProblemsList = adminObj.GetAllReportedProblems();
            return problemObj;
        }

        public WorkDetailsVm GetAdminWorkDetails()
        {
            var adminWorkDetails = new WorkDetailsVm();
            adminWorkDetails.CityList = listRepo.FillCity();
            adminWorkDetails.StateList = listRepo.FillState();
            adminWorkDetails.DepartmentsList = listRepo.FillDepartments();
            return adminWorkDetails;
        }

        public List<WorkDetailsVm> GetGroupMembers(int cityId)
        {
            List<WorkDetailsVm> groupMembersList = listRepo.BindGroupMembers(cityId);
            return groupMembersList;
        }

        public bool AssignWorkDetails(WorkDetailsVm assignedWork)
        {
            bool status = adminObj.SaveAssignedWorkDetails(assignedWork);
            return status;
        }


        public WorkDetailsVm GetWorksByDepartment(int selectedDomainId)
        {
            var userWorkListObj = new WorkDetailsVm();
            userWorkListObj.WorksList = adminObj.GetAllWork();
            userWorkListObj.WorksList.RemoveAll(i => i.DeptId != selectedDomainId);
            return userWorkListObj;
        }

        public UserDetails SearchByUserName(string userName)
        {
            var userObj = new UserDetails();
            userObj.UsersList = adminObj.SearchUsersByName(userName);
            return userObj;
        }

    }
}

