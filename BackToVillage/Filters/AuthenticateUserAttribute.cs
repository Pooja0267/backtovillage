﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BackToVillage.Filters
{
    public class AuthenticateUserAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.Current.Session["user"] == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary 
                                                                        { 
                                                                            { "controller", "UserAccount" }, 
                                                                            { "action", "Login" } 
                                                                        });
            }
        }
    }
}



