﻿using BackToVillage.BusinessLayer;
using System;
using System.Web;
using System.Web.Mvc;

namespace BackToVillage.Filters
{
    public class AuthorizeUserRoleAttribute: AuthorizeAttribute
    {       
        private readonly string[] _userRoles ;

        public AuthorizeUserRoleAttribute(params string[] allowedRoles)  
        {  
           this._userRoles = allowedRoles;  
        } 

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {        
            var roleName=String.Empty;

            if (HttpContext.Current.Session["user"] != null)
            {
                var userAccount = new AccountBl();

                int userId = (int)HttpContext.Current.Session["user"];              
                var roleId = userAccount.GetRole(userId);
                return userAccount.IsUserValid(roleId, _userRoles);               
            }
            return false;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)  
        {
            //filterContext.Result = new PartialViewResult()
            //{
            //    ViewName = "_UnAuthorizedAccess"
            //};

            filterContext.Result = new ViewResult()
            {
                ViewName = "UnAuthorizedAccess"
            };
        }
    }
}
