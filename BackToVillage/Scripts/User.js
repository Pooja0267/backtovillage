﻿

$(document).ready(function () {
    BindEvents();
});


function BindEvents() {

    //State list On Change Event 
    $("#stateList")
        .change(function () {
            BindCitiesFromState($(this).val(), $("#cityList"));
        });

    $("#cityList")
        .change(function () {
            BindGroupMembers($(this).val(), $("#groupMemberList"))
        });

    $("#editedDob")
        .focusin(function () {
            $("#editedDob").prop("type", "date");
        });

    //When Work Status is set to Start
    $("#workStatusList")
        .change(function () {
            if ($(this).val() == 2) {
                $("#startingDateLabel").css("display", "block");
                $("#startingDate").css("display", "block");
            } else if ($(this).val() == 4) {
                $("#startingDateLabel").css("display", "none");
                $("#startingDate").css("display", "none");
            } else if ($(this).val() == 3) {
                $("#startingDateLabel").css("display", "none");
                $("#startingDate").css("display", "none");
            }

        });

    //When Progress status is loaded
    $("#workUpdateForm")
        .load(function () {
            var status = $("#progressStatus").val();
            $("#progressbar").css("width", status + '%');
        });

    //When Progress status is changed
    $("#progressStatus")
        .focusout(function () {
            var status = $("#progressStatus").val();
            $("#progressbar").css("width", status + '%');
        });

    //To Filter User Works Web Grid Based on Work Status
    $("#workStatusListFilter")
        .change(function () {
            FilterWebGrid($(this).val());
        });

    //To Filter User Works Web Grid Based on Work Status By Admin
    $("#workStatusListFilterAdmin")
        .change(function () {
            FilterWebGridAdmin($(this).val());
        });

    //To Filter User Works Web Grid Based on Department By Admin
    $("#filterGridByDepartmentAdmin")
        .change(function () {
            FilterWebGridBasedOnDepartment($(this).val());
        });

    // Set Color of Column Based on Work Status
    $('#gridMapping > tbody > tr')
        .each(function (index) {

            if ($(this).children('td:nth-child(5)').text() == "Pending") {
                $(this).children('td:nth-child(5)').css("background-color", "#FF6133");
                $(this).children('td:nth-child(6)').css("pointer-events", "none", "cursor", "default");
                $(this).children('td:nth-child(7)').css("pointer-events", "none", "cursor", "default");
                $(this).children('td:nth-child(8)').css("pointer-events", "none", "cursor", "default");
            } else if (($(this).children('td:nth-child(5)').text() == "Initialised")) {
                $(this).children('td:nth-child(5)').css("background-color", "#FFCC99");
                $(this).children('td:nth-child(7)').css("pointer-events", "none", "cursor", "default");
                $(this).children('td:nth-child(8)').css("pointer-events", "none", "cursor", "default");
            } else if (($(this).children('td:nth-child(5)').text() == "Started")) {
                $(this).children('td:nth-child(5)').css("background-color", "#FFCC99");
                $(this).children('td:nth-child(7)').css("pointer-events", "none", "cursor", "default");
                $(this).children('td:nth-child(8)').css("pointer-events", "none", "cursor", "default");
            } else if (($(this).children('td:nth-child(5)').text() == "InProgress")) {
                $(this).children('td:nth-child(5)').css("background-color", "#83FF33");
                $(this).children('td:nth-child(7)').css("pointer-events", "none", "cursor", "default");
                $(this).children('td:nth-child(8)').css("pointer-events", "none", "cursor", "default");
            } else if (($(this).children('td:nth-child(5)').text() == "Rejected")) {
                $(this).children('td:nth-child(5)').css("background-color", "#33E6FF");
                $(this).children('td:nth-child(6)').css("pointer-events", "none", "cursor", "default");
                $(this).children('td:nth-child(8)').css("pointer-events", "none", "cursor", "default");
            } else if (($(this).children('td:nth-child(5)').text() == "Closed")) {
                $(this).children('td:nth-child(5)').css("background-color", "#FF333F");
                $(this).children('td:nth-child(6)').css("pointer-events", "none", "cursor", "default");
            }
        });

    $("#refreshUsersGrid")
        .click(function () {
            $.get('../Admin/RefreshAllUsers',
                null,
                function (result) {
                    $('#gridContent').html(result);
                    $("#userName").val("");
                });
        });

    $("#refreshAdminWorksGrid")
        .click(function () {
            $.get('../Admin/RefreshAllWorks',
                null,
                function (result) {
                    $('#gridContent').html(result);
                });
        });

    $("#refreshUserWorksGrid")
        .click(function () {
            $.get('../User/RefreshWorks',
                null,
                function (result) {
                    $('#gridContent').html(result);
                });
        });


    $("#searchByUsername")
        .submit(function () {
            SearchByUserName();
            return false;
        });

    //$("#userDashBoard")
    //    .click(function() {
    //        $.ajax({
    //            url: 'DashBoard',
    //            method: 'get',
    //            dataType: 'html',
    //            success: function(result) {
    //                $('#bodyContent').html("");
    //                $('#bodyContent').append(result);
    //                alert("Ajax Working");
    //            },
    //            error: function (error) { alert("Script Error"); }
    //        });
    //    });

    //$("#addwork")
    //    .click(function() {
    //        $.ajax({
    //            url: 'AddWork',
    //            method: 'get',
    //            dataType: 'html',
    //            success: function(result) {
    //                $('#bodyContent').html("");
    //                $('#bodyContent').append(result);
    //                //alert("Ajax Working");
    //            },
    //            error: function (error) { alert("Script Error"); }
    //        });
    //    });

    //$("#userViewWorks")
    //    .click(function () {
    //        $.ajax({
    //            url: 'ViewWorks',
    //            method: 'get',
    //            dataType: 'html',
    //            success: function (result) {
    //                $('#bodyContent').html("");
    //                $('#bodyContent').append(result);
    //               // alert("Ajax Working"); 
    //            },
    //            error: function (error) { alert("Script Error!"); }
    //        });

    //        //To Filter User Works Web Grid Based on Work Status
           
    //    });

    //$("#reportAnIssue")
    //    .click(function() {
    //        $.ajax({
    //            url: 'ReportAnIssue',
    //            method: 'get',
    //            dataType: 'html',
    //            success: function(result) {
    //                $('#bodyContent').html("");
    //                $('#bodyContent').append(result);
    //                //alert("Ajax Working");
    //            },
    //            error: function (error) { alert("Script Error!"); }
    //    });
    //    });

    //$("#editProfile")
    //    .click(function() {
    //        $.ajax({
    //            url: '/UserAccount/Edit',
    //            method: 'get',
    //            dataType: 'html',
    //            success: function(result) {
    //                $('#bodyContent').html("");
    //                $('#bodyContent').append(result);
    //                //alert("Ajax Working");
    //            },
    //            error: function (error) { alert("Script Error!"); }  
    //    });
    //    });

    //$("#changePassword")
    //    .click(function() {
    //        $.ajax({
    //            url: '/UserAccount/ChangePassword',
    //            method: 'get',
    //            dataType: 'html',
    //            success:function(result) {
    //                $("#bodyContent").html("");
    //                $("#bodyContent").append(result);
    //               // alert("Ajax Working");
    //            },
    //            error:function (error) { alert("Script Error !"); }
    //        });
    //    });

    //$("#changePasswordSubmit")
    //    .click(function () {
    //        var myObj = {};
    //        myObj.Name = 
    //        $.ajax({
    //            url:'myWebsite',
    //            data: { myMethodParameter : myObject},	//Data passing
    //            dataType: "JSON",
    //            Method: 'POST',
    //            success: function(data){},
    //            error: function(data) { alert("Failed. Try Again."); }
    //        });
    //    });

    // Search By UserName
    function SearchByUserName() {
        var userName = $("#userName").val();
        $.get('../Admin/SearchByUserName',
            { userName: userName },
            function (result) {
                $('#gridContent').html(result);
            });
    }

    // Function to Bind Cities based on State Selection
    function BindCitiesFromState(stateId, selector) {

        $.getJSON("../UserAccount/LoadCitiesByState",
            { stateId: stateId != null && stateId != " " ? Number(stateId) : 0 },
            function (data) {
                var selectElement = selector;
                selectElement.empty();
                selectElement.append($('<option/>',
                {
                    value: 0,
                    text: "Select City"
                }));
                $.each(data,
                    function (index, itemData) {
                        selectElement.append($('<option/>',
                        {
                            value: itemData.Value,
                            text: itemData.Text
                        }));
                    });
            });
    }

    // Function to Bind Group Members based on City Selection
    function BindGroupMembers(cityId, selector) {

        $.getJSON("../Admin/LoadGroupMembersByCity",
            { cityId: cityId },
            function (data) {
                var selectElement = selector;
                selectElement.empty();
                selectElement.append($('<option/>',
                {
                    value: 0,
                    text: "Select Group Member"
                }));
                $.each(data,
                    function (index, itemData) {
                        selectElement.append($('<option/>',
                        {
                            value: itemData.Value,
                            text: itemData.Text
                        }));
                    });
            });
    }


    //To Filter User Works Web Grid Based on Work Status
    function FilterWebGrid(selectedWorkStatusId) {
        $.get('../User/FilterByWorkStatus',
            { selectedWorkStatusId: selectedWorkStatusId },
            function (result) {
                $('#gridContent').html(result);
            });
    }

    //To Filter All Works Web Grid Based on Work Status by Admin
    function FilterWebGridAdmin(selectedWorkStatusId) {

        $.get('../Admin/FilterByWorkStatus',
            { selectedWorkStatusId: selectedWorkStatusId },
            function (result) {
                $('#gridContent').html(result);
            });
    }

    //To Filter All Works Web Grid Based on Department by Admin
    function FilterWebGridBasedOnDepartment(selectedDomainId) {
        $.get('../Admin/FilterByDepartment',
            { selectedDomainId: selectedDomainId },
            function (result) {
                $('#gridContent').html(result);
            });

    }

    //Approve a Work Request
    function ApproveWorkRequest(linkId) {
        confirm('Are you sure you approve this request?');
        $("#rejectWorkRequest").css("pointer-events", "none", "cursor", "default");
        $("#approveWorkRequest").css("pointer-events", "none", "cursor", "default", "text", "Approved");
    }

    //Reject a Work Request
    function RejectWorkRequest(linkId) {
        confirm('Are you sure you want to reject this request?');
        $("#rejectWorkRequest").css("pointer-events", "none", "cursor", "default", "text", "Rejected");
        $("#approveWorkRequest").css("pointer-events", "none", "cursor", "default");
    }


    //Approve a Group Member Request
    function ApproveGroupMemberRequest(linkId) {
        confirm('Are you sure you approve this request?');
        $("#rejectGroupMemberRequest").css("pointer-events", "none", "cursor", "default");
        $("#approveGroupMemberRequest").css("pointer-events", "none", "cursor", "default").text("Approved");
    }

    //Reject a Group Member Request
    function RejectGroupMemberRequest(linkId) {
        confirm('Are you sure you want to reject this request?');
        $("#rejectGroupMemberRequest").css("pointer-events", "none", "cursor", "default").text("Rejected");
        $("#approveGroupMemberRequest").css("pointer-events", "none", "cursor", "default");
    }
   
}
function postForm() {
    var obj = {
        ProgressId: $("#workStatusList").val(),
        ProgressStatus: $("#progressStatus").val(),
        UsedFunds: $("#usedfunds").val(),
        Comment: $("#comment").val(),
        WorkId: $("#workId").val()
        // $("#startingDate").val,
    }

    $.ajax({
        url: '/User/UpdateWork',
        method: 'post',
        data: obj,
        success: function (stringResult) {
            $("#updateWorkForm").html("");
            $("#msg").append(stringResult);
        },
        error: function (error) { alert("Something went wrong."); }
    });

}


function getModal(workId) {

    $.ajax({
        url: '/User/UpdateWork',
        data: { workId: workId },
        method: 'get',
        dataType: 'html',
        success: function (result) {
            // Get the modal
            var modal = document.getElementById('myModal');
            modal.style.display = "block";
            $("#msg").html("");
            $("#updateWorkForm").html("");
            $("#updateWorkForm").append(result);

            //When Progress status is changed
            $("#progressStatus")
                .focusout(function () {
                    var status = $("#progressStatus").val();
                    $("#progressbar").css("width", status + '%');
                });

            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on <span> (x), close the modal
            span.onclick = function () {
                modal.style.display = "none";
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }
        },
        error: function (error) { alert("Something went wrong."); }
    });

}