﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackToVillage.Utilities
{
    public static class  BTV_Constants
    {
        public const string RegistrationSuccess = "Congratulations !!! You are successfully registered.";

        public const string RegistrationFailure = "Sorry !!! You could not be registered.Please Try again later";

        public const string EmailExists = "This EmailId already exists.";

        public const string ModelError = "Something went wrong.Please try again later.";

        public const string LoginFail = "Invalid Credentials";

        public const string PhotoUploadError = "Some error occured while uploading..!";

        public const string EditProfileSuccess =  "Congratulations !! Your Profile has been updated Successfully.";

        public const string PasswordChangeSuccess = "Your Password has been changed Successfully";

        public const string AccountDeleted = "Account Deleted Successfully";

        public const string DefaultMaleProfileImage = "D:\\Projects\\ProfilePictures\\profile-pic.jpg";

        public const string DefaultFemaleProfileImage = "D:\\Projects\\ProfilePictures\\female_cartoon_avatar.png";

        public const string WorkAddedSuccess = "Your Work has been Added Successfully";

        public const string Error = "Some Error Occured. Please Try again Later.";

        public const string NoWorkAdded = "No Work has been added yet.";

        public const string WorkRemoved = "Work Has been Removed";

        public const string WorkUpdateSuccess = "Work Has been Updated";

        public const string WorkUpdateFailure = "Work Could Not Be Updated.Try Again. ";

        public const string StorySuccess = "Your Story has been Successfully Published";

        public const string StoryFailure = "Story could not be published.Try Again. ";

        public const string ProblemReportSuccess = "Your Problem has been registered.";

        public const string ProblemReportFail = "Your Problem could not be registered.Try Again Later. ";

        public const string WorkApproved = "Work Approved";

        public const string WorkRejected = "Work Rejected";

        public const string WorkAssigned = "The Work has been successfully assigned.";

    }
}