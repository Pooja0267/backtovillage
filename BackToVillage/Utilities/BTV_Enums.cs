﻿
namespace BackToVillage.Utilities
{
        enum UserRoles : int 
        {
            Admin = 1,
            GroupMember = 2,
            User =3
        }

        enum WorkStatus
        {
            Initialised = 1, 
            Started = 2,
            InProgress = 3, 
            Closed = 4,
            Pending= 5,
            Rejected = 6
        }
    
}
