﻿using BackToVillage.Models;
using BackToVillage.ViewModels;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BackToVillage.DataAccessLayer
{
    public class AdminDal : DbConnection
    {
        //To view employee details with generic list       
        public List<UserDetails> GetGroupMemberRequests()
        {
            try
            {
                using (var conn = GetConnection())
                {

                    var userList = conn.Query<UserDetails>("GroupMemberRequestsList",
                                                            commandType: CommandType.StoredProcedure).ToList();
                    return userList;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return null;
        }


        // Change User Role
        public void ApproveRole(string emailId)
        {
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Query<User>("ApproveGroupMember", new { @Email = emailId },
                                      commandType: CommandType.StoredProcedure);
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        // Change User Role 
        public void RejectRole(string emailId)
        {
            using (var conn = GetConnection())
            {
                conn.Query<User>("RejectGroupMember", new { @Email = emailId }, commandType: CommandType.StoredProcedure);
            }
        }

        public List<WorkDetailsVm> GetWorkRequests()
        {
            try
            {
                using (var conn = GetConnection())
                {
                    var workList = conn.Query<WorkDetailsVm>("WorkRequestList",
                                                             commandType: CommandType.StoredProcedure).ToList();

                    return workList;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return null;
        }

        public void ApproveWorkRequest(WorkDetailsVm work)
        {
            try
            {
                using (var conn = GetConnection())
                {
                    var workDetails = new DynamicParameters();
                    workDetails.Add("@workId", work.WorkId);
                    workDetails.Add("@FundGranted", work.FundGranted);
                    workDetails.Add("@ProgressId", 1);
                    conn.Query<WorkDetailsVm>("ApproveWorkRequest", new
                    {
                        @workId = work.WorkId,
                        @FundGranted = work.FundGranted,
                        @ProgressId = 1
                    },
                                                commandType: CommandType.StoredProcedure);
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public void RejectWorkRequest(int workId)
        {
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Query<WorkDetailsVm>("RejectWorkRequest", new { @workId = workId },
                                               commandType: CommandType.StoredProcedure);
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        //To view employee details with generic list       
        public List<UserDetails> GetAllUsers()
        {
            try
            {
                using (var conn = GetConnection())
                {
                    var userList = conn.Query<UserDetails>("GetAllUsersList",
                                                            commandType: CommandType.StoredProcedure).ToList();
                    return userList;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return null;
        }


        //To view employee details with generic list       
        public List<WorkDetailsVm> GetAllWork()
        {
            try
            {
                using (var conn = GetConnection())
                {
                    var workList = conn.Query<WorkDetailsVm>("AllWorksList",
                                                             commandType: CommandType.StoredProcedure).ToList();
                    return workList;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return null;
        }

        //To view all reported problems in generic list       
        public List<ProblemIssuesVm> GetAllReportedProblems()
        {
            try
            {
                using (var conn = GetConnection())
                {

                    var problemList = conn.Query<ProblemIssuesVm>("AllProblemsList",
                                                                   commandType: CommandType.StoredProcedure).ToList();
                    return problemList;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return null;
        }

        public bool SaveAssignedWorkDetails(WorkDetailsVm assignedWork)
        {
            bool status;
            try
            {
                using (var conn = GetConnection())
                {
                    status = conn.Execute("AssignWork", new
                    {
                        @Title = assignedWork.Title,
                        @Aim = assignedWork.Aim,
                        @FK_DeptId = assignedWork.SelectedDomainId,
                        @FK_StateId = assignedWork.SelectedStateId,
                        @FK_CityId = assignedWork.SelectedCityId,
                        @FundsGranted = assignedWork.FundGranted,
                        @FK_ProgressId = 1,
                        @Userid = assignedWork.SelectedGroupMemberId
                    },
                                                              commandType: CommandType.StoredProcedure) > 0;
                }
            }


            catch (Exception ex)
            {
                status = false;
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return status;
        }

        public List<UserDetails> SearchUsersByName(string userName)
        {
            try
            {
                using (var conn = GetConnection())
                {
                    var userList = conn.Query<UserDetails>("SearchUsersByName", new { @userName = userName }, commandType: CommandType.StoredProcedure).ToList();
                    return userList;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return null;
        }
    }
}