﻿using BackToVillage.Models;
using BackToVillage.ViewModels;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BackToVillage.DataAccessLayer
{
    public class ListsDal : DbConnection
    {
        public List<City> FillCity()
        {
            using (var conn = GetConnection())
            {
                var cities = conn.Query<City>("CitiesList", commandType: CommandType.StoredProcedure).ToList();
                return cities;
            }
        }

        public List<City> BindCity(int stateId)
        {
            using (var conn = GetConnection())
            {
                var cities = conn.Query<City>("CitiesByStateId", new { @stateId = stateId }, 
                                                commandType: CommandType.StoredProcedure).ToList();
                return cities;
            }
        }

        public List<Departments> FillDepartments()
        {
            using (var conn = GetConnection())
            {
                var result = conn.Query<Departments>("Departments", commandType: CommandType.StoredProcedure).ToList();
                return result;
            }
        }

        public List<Occupation> FillOccupation()
        {
            using (var conn = GetConnection())
            {
                var occupation = conn.Query<Occupation>("Occupations", commandType: CommandType.StoredProcedure).ToList();
                return occupation;
            }
        }

        public List<State> FillState()
        {
            using (var conn = GetConnection())
            {
                var state = conn.Query<State>("States", commandType: CommandType.StoredProcedure).ToList();
                return state;
            }
        }

        public List<Works> FillWorkStatus()
        {
            using (var conn = GetConnection())
            {
                var workStatus = conn.Query<Works>("WorkStatusList", commandType: CommandType.StoredProcedure).ToList();
                return workStatus;
            }
        }

        public List<WorkDetailsVm> BindGroupMembers(int cityId)
        {
            using (var conn = GetConnection())
            {
                var groupMembers = conn.Query<WorkDetailsVm>("GroupMembersByCityId", new { @cityId = cityId }, 
                                                              commandType: CommandType.StoredProcedure).ToList();
                return groupMembers;
            }
        }

        public List<WorkDetailsVm> GetSuccessStories()
        {
            using (var conn = GetConnection())
            {
                var stories = conn.Query<WorkDetailsVm>("SuccessStories", commandType: CommandType.StoredProcedure).ToList();
                return stories;
            }
        }
    }
}