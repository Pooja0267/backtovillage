﻿using BackToVillage.ViewModels;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BackToVillage.DataAccessLayer
{
    public class UserDal : DbConnection
    {
        public bool AddWork(WorkDetailsVm work)
        {
            bool status;

            try
            {
                using (var conn = GetConnection())
                {

                    status = conn.Execute("AddWork", new
                    {
                        @Title = work.Title,
                        @Aim = work.Aim,
                        @FK_DeptId = work.SelectedDomainId,
                        @FK_StateId = work.SelectedStateId,
                        @FK_CityId = work.SelectedCityId,
                        @FundsRequired = work.FundsRequired,
                        @FK_ProgressId = 5,
                        @Userid = work.UserId
                    },
                                                          commandType: CommandType.StoredProcedure) > 0;
                }
            }

            catch (Exception ex)
            {
                status = false;
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return status;
        }

        public List<WorkDetailsVm> GetWorkList(int userId)
        {
            try
            {
                using (var conn = GetConnection())
                {
                    var workList = conn.Query<WorkDetailsVm>("WorkListByUser", new { @userId = userId }, 
                                                              commandType: CommandType.StoredProcedure).ToList();
                    return workList;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return null;
        }


        public void RemoveWork(int userId)
        {
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Query("RemoveWork", new { @UserId = userId }, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

        }


        public WorkDetailsVm GetWorkStatus(int workId)
        {
            try
            {
                using (var conn = GetConnection())
                {
                    var workdetails = conn.Query<WorkDetailsVm>("WorkDetails", new { @WorkId = workId }, 
                                                                commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return workdetails;
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return null;

        }


        public bool UpdateWork(WorkDetailsVm work)
        {
            bool status;
            try
            {
                using (var conn = GetConnection())
                {
                    status = conn.Execute("UpdateWork", new
                    {
                        @StartingDate = work.StartingDate,
                        @EndingDate = work.EndingDate,
                        @WorkId = work.WorkId,
                        @ProgressId = work.ProgressId,
                        @ProgressStatus = work.ProgressStatus,
                        @Comment = work.Comment,
                        @UsedFunds = work.UsedFunds
                    },
                                                            commandType: CommandType.StoredProcedure) > 0;
                }
            }
            catch (Exception ex)
            {
                status = false;
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return status;
        }

        public WorkDetailsVm WorkDetails(int workId)
        {
            try
            {
                using (var conn = GetConnection())
                {
                    var workdetails = conn.Query<WorkDetailsVm>("WorkDetails", new { @WorkId = workId },
                                                                commandType: CommandType.StoredProcedure).FirstOrDefault();
                    conn.Close();
                    return workdetails;
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return null;
        }

        public bool AddSuccessStory(WorkDetailsVm workStory)
        {
            bool status;
            try
            {
                using (var conn = GetConnection())
                {
                    status = conn.Execute("AddWorkSuccessStory", new { 
                                                                        @StoryTitle = workStory.StoryTitle, 
                                                                        @workId = workStory.WorkId, 
                                                                        @StorySummary = workStory.StorySummary  
                                                                      },
                                                                      commandType: CommandType.StoredProcedure) > 0;
                }
            }
            catch (Exception ex)
            {
                status = false;
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return status;
        }

        public bool AddProblem(ProblemIssuesVm problemObj)
        {
            bool status;
            try
            {
                using (var conn = GetConnection())
                {
                    var userProblemParameter = new DynamicParameters();


                    status = conn.Execute("AddAnIssue", new
                    {
                        @UserName = problemObj.UserName,
                        @UserId = problemObj.UserId,
                        @ProblemDetails = problemObj.ProblemDetails,
                        @StateId = problemObj.SelectedStateId,
                        @CityId = problemObj.SelectedCityId,
                        @DeptId = problemObj.SelectedDomainId,
                        @ReportingDate = problemObj.ReportedOn
                    },
                                                              commandType: CommandType.StoredProcedure) > 0;
                }
            }
            catch (Exception ex)
            {
                status = false;
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return status;
        }
    }
}
