﻿using BackToVillage.ViewModels;
using Dapper;
using System;
using System.Data;
using System.Linq;

namespace BackToVillage.DataAccessLayer
{
    public class AccountDal : DbConnection
    {
        public bool CheckEmail(string email)
        {
            using (var conn = GetConnection())
            {
                bool result = conn.ExecuteScalar<bool>("CheckEmail", new { @Email = email },
                                                        commandType: CommandType.StoredProcedure);
                conn.Close();
                return result;
            }
        }

        //Adding the users
        public bool AddUser(UserDetails objUser)
        {
            bool status = false;
            try
            {
                using (var conn = GetConnection())
                {

                    status = conn.Execute("UserRegister", new
                    {
                        @FirstName = objUser.FirstName,
                        @LastName = objUser.LastName,
                        @Email = objUser.Email,
                        @DOB = objUser.DOB,
                        @Gender = objUser.Gender,
                        @PhoneNumber = objUser.PhoneNumber,
                        @AltPhoneNumber = objUser.AltPhoneNumber,
                        @FK_CityId = objUser.SelectedCityId,
                        @FK_StateId = objUser.SelectedStateId,
                        @FK_OccupationId = objUser.SelectedOccupationId,
                        @FK_RoleId = 3,
                        @Address = objUser.Address,
                        @RoleSelection = objUser.isSelected,
                        @Password = objUser.HashPassword
                    },
                                                       commandType: CommandType.StoredProcedure) >0 ;

                    return status;
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return false;
        }

        public int MatchUserCredentials(LoginVm objUser)
        {
            try
            {
                using (var conn = GetConnection())
                {
                    var result = conn.ExecuteScalar<int>("UserCredentials", new
                    {
                        @Username = objUser.Email,
                        @Password = objUser.HashPassword
                    },
                                                                                commandType: CommandType.StoredProcedure);
                    return result;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return 0;
        }

        public int GetRoleId(int userId)
        {
            using (var conn = GetConnection())
            {
                var roleId = conn.ExecuteScalar<int>("RoleId", new { @userId = userId },
                                                                       commandType: CommandType.StoredProcedure);

                return roleId;
            }
        }
        public void SavePath(string path, int userid)
        {
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Execute("SaveProfilePhotoPath", new
                    {
                        @Path = path,
                        @UserId = userid
                    },
                                                               commandType: CommandType.StoredProcedure);
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public UserDetails GetUserDetails(int userId)
        {
            try
            {
                using (var conn = GetConnection())
                {
                    var userDetails = conn.Query<UserDetails>("UserDetails", new { @UserId = userId },
                                                              commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return userDetails;
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return null;
        }
        public int GetUserId(string emailId)
        {
            try
            {
                if (String.IsNullOrEmpty(emailId) != true)
                {
                    using (var conn = GetConnection())
                    {
                        var userId = conn.Query<int>("GetUserId", new { @userName = emailId },
                                                     commandType: CommandType.StoredProcedure).FirstOrDefault();
                        return userId;
                    }
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return 0;
        }

        public void deleteUser(int userId)
        {
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Query("DeleteUser", new { @userId = userId }, commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public void ChangePassword(int userId, string password)
        {
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Query("ChangePassword", new
                    {
                        @Userid = userId,
                        @Password = password
                    },
                                                       commandType: CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public void EditUser(EditProfile objUser)
        {
            try
            {
                using (var conn = GetConnection())
                {
                    conn.Execute("EditUserProfile", new
                    {
                        @FirstName = objUser.FirstName,
                        @LastName = objUser.LastName,
                        @Email = objUser.Email,
                        @DOB = objUser.DOB,
                        @PhoneNumber = objUser.PhoneNumber,
                        @AltPhoneNumber = objUser.AltPhoneNumber,
                        @FK_CityId = objUser.SelectedCityId,
                        @FK_StateId = objUser.SelectedStateId,
                        @FK_OccupationId = objUser.SelectedOccupationId,
                        @Address = objUser.Address,
                        @UserId = objUser.UserId
                    },
                                                        commandType: CommandType.StoredProcedure);
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public EditProfile GetEditUserDetails(int userId)
        {
            try
            {
                using (var conn = GetConnection())
                {
                    var userDetails = conn.Query<EditProfile>("UserDetails", new { @userId = userId },
                                                              commandType: CommandType.StoredProcedure).FirstOrDefault();
                    return userDetails;
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return null;
        }

    }
}