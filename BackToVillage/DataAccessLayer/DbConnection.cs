﻿using System.Configuration;
using System.Data.SqlClient;

namespace BackToVillage.DataAccessLayer
{
    public class DbConnection
    {
        private string connString;
        private SqlConnection connection;

        protected DbConnection()
        {
            connString = ConfigurationManager.ConnectionStrings["DemoProject"].ConnectionString;
        }
        
        protected SqlConnection GetConnection()
        {
            connection = new SqlConnection(connString);
            connection.Open();
            return connection;
        }
    }
}


