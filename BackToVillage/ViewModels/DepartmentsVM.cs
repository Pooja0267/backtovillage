﻿
namespace BackToVillage.ViewModels
{
    public class DepartmentsVm
    {
        public int DeptId { get; set; }
        public string DeptName { get; set; }
        public int SelectedDeptId { get; set; }
    }
}