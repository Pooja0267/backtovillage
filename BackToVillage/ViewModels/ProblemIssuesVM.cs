﻿using BackToVillage.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BackToVillage.ViewModels
{
    public class ProblemIssuesVm
    {
        public int? ProblemId { get; set; }

        [Required(ErrorMessage = "This Field is Required.")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Problem Details")]
        public string ProblemDetails { get; set; }

        public int? UserId { get; set; }
        public string UserName { get; set; }

        //City
        public int? CityId { get; set; }

        [Display(Name = "City")]
        public string CityName { get; set; }

        [Display(Name = "City")]
        public List<City> CityList { get; set; }

        [Required(ErrorMessage = "Please select a city.")]
        public int? SelectedCityId { get; set; }

        //State
        public int? StateId { get; set; }

        [Display(Name = "State")]
        public string StateName { get; set; }

        [Display(Name = "State")]
        public List<State> StateList { get; set; }

        [Required(ErrorMessage = "Please select a state.")]
        public int? SelectedStateId { get; set; }

        //Departments
        public int? DeptId { get; set; }

        [Display(Name = "Department")]
        public string DeptName { get; set; }

        [Required(ErrorMessage = "Please select the Domain of Work.")]
        public int? SelectedDomainId { get; set; }

        [Display(Name = "Department")]
        public List<Departments> DepartmentsList { get; set; }

        public List<ProblemIssuesVm> ProblemsList { get; set; }

        [Display(Name = "Reported On")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ReportedOn { get; set; }
    }
}