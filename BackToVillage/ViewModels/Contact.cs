﻿using System.ComponentModel.DataAnnotations;

namespace BackToVillage.ViewModels
{
    public class Contact
    {
        [Required(ErrorMessage = "Please enter your name.")]
        [Display(Name = "Name")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "The Name should be between 2-50 characters.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter an email address.")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "The Email Address is not valid.")]
        public string Email { get; set; }

        [DataType(DataType.MultilineText)]
        [StringLength(500, MinimumLength = 20, ErrorMessage = "The Message should be between 20-500 characters.")]
        public string Message { get; set; }
    }
}