﻿using BackToVillage.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BackToVillage.ViewModels
{
    
    public class WorkDetailsVm
    {
        [StringLength(100, MinimumLength = 15, ErrorMessage = "The Title should be between 15-100 characters.")]
        [Required(ErrorMessage = "Please enter a suitable Title.")]
        public string Title { get; set; }

        [StringLength(100, MinimumLength = 15, ErrorMessage = "The First Name should be between 15-100 characters.")]
        [Required(ErrorMessage = "Please enter the Aim of the Work.")]
        public string Aim { get; set; }

        public int? DeptId { get; set; }

        [Display(Name = "Department")]
        public string DeptName { get; set; }

        [Required(ErrorMessage = "Please select the Domain of Work.")]
        public int? SelectedDomainId { get; set; }

        [Display(Name = "Department")]
        public List<Departments> DepartmentsList { get; set; }

        [Required(ErrorMessage = "Please enter the Required Fund.")]
        [Display(Name = "Required Fund")]
        public float FundsRequired { get; set; }

        [Display(Name = "Fund Granted")]
        public float FundGranted { get; set; }

        [Display(Name = "Remaining Fund")]
        public float RemainingFund { get; set; }

        [Display(Name = "Fund Used (Till date)")]
        public float UsedFunds { get; set; }

        public int? CityId { get; set; }

        [Display(Name = "City")]
        public string CityName { get; set; }

        [Display(Name = "City")]
        public List<City> CityList { get; set; }

        [Required(ErrorMessage = "Please select a city.")]
        public int? SelectedCityId { get; set; }

        public int? StateId { get; set; }
        public string StateName { get; set; }

        [Display(Name = "State")]
        public List<State> StateList { get; set; }

        [Required(ErrorMessage = "Please select a state.")]
        public int? SelectedStateId { get; set; }

        
        public int? ProgressId { get; set; }

        [Display(Name="Work Status")]
        public string WorkStatus { get; set; }

        [Display(Name = "Work Status")]
        public List<Works> WorkStatusList { get; set; }
        public int? SelectedWorkStatusId { get; set; }

        
        public int? UserId { get; set; }
        public string UserName { get; set; }

        public List<WorkDetailsVm> WorksList { get; set; }

        public int WorkId { get; set; }

        // For Progress Bar
        public int? ProgressStatus { get; set; }

        [DataType(DataType.MultilineText)]
        public string Comment { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Starting Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartingDate { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Ending Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EndingDate { get; set; }

        [Display(Name= "Success Story Title")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "The Story Title should be between 2-50 characters.")]
        public String StoryTitle { get; set; }

        [Display(Name = "Story Summary")]
        [DataType(DataType.MultilineText)]
        [StringLength(500, MinimumLength = 50, ErrorMessage = "The Story Summary should be between 50-500 characters.")]
        public String StorySummary { get; set; }

        public int? ProblemId { get; set; }

        public List<UserDetails> GroupMembersList { get; set; }
        public int? SelectedGroupMemberId { get; set; }

        public List<WorkDetailsVm> SuccessStoriesList { get; set; }
    }
}

       