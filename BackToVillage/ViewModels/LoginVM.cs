﻿using System.ComponentModel.DataAnnotations;

namespace BackToVillage.ViewModels
{
    public class LoginVm
    {
        [Required(ErrorMessage = "Please enter your Email Id.")]
        [Display(Name = "Username")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "The Email Address is not valid")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter your Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public string HashPassword { get; set; }
    }
}