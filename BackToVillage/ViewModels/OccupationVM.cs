﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackToVillage.ViewModels
{
    public class OccupationVm
    {
        public int OccupationId { get; set; }
        public string Occupation { get; set; }
        public int SelectedOccupationId { get; set; }
    }
}