﻿
namespace BackToVillage.View_Models
{
    public class CityVm
    {
        public int CityId { get; set; }
        public string CityName { get; set; } 
        public int SelectedCityId { get; set; }

        public int StateId { get; set; }
    }
}

