﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackToVillage.ViewModels
{
    public class ComplexUserModel
    {
        public UserDetails UserDetails { get; set; }
        public WorkDetailsVm WorkDetails { get; set; }
    }
}