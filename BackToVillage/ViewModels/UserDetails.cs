﻿using BackToVillage.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BackToVillage.ViewModels
{
    public class UserDetails
    {
        public int? UserId { get; set; }

        [Display(Name = "Name")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Please enter your first name.")]
        [Display(Name = "First Name")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "The First Name should be between 2-50 characters.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter your last name.")]
        [StringLength(50, ErrorMessage = "The Last Name should be between 2-50 characters.")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter an email address.")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "The Email Address is not valid.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter your Date of Birth.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime DOB { get; set; }

        public int? Age { get; set; }

        [Required(ErrorMessage = "Please enter your 10-digit Mobile number.")]
        [Display(Name = "Mobile No.")]
        [DataType(DataType.PhoneNumber)]
        [StringLength(10)]
        public string PhoneNumber { get; set; }

        [Display(Name = "Alternate Phone No. ")]
        public string AltPhoneNumber { get; set; }

        [Required(ErrorMessage = "Please enter your address.")]
        public string Address { get; set; }

        [Required(ErrorMessage="Please select your Gender.")]
        public string Gender { get; set; }


        public int? CityId { get; set; }

        [Display(Name = "City")]
        public string CityName { get; set; }

        [Display(Name = "City")]
        public List<City> CityList { get; set; }

        [Required(ErrorMessage = "Please select a city.")]
        public int? SelectedCityId { get; set; }

        public int? StateId { get; set; }

        [Display(Name = "State")]
        public string StateName { get; set; }

        [Display(Name = "State")]
        public List<State> StateList { get; set; }

        [Required(ErrorMessage = "Please select a state.")]
        public int? SelectedStateId { get; set; }

        public int? OccupationId { get; set; }

        [Display(Name = "Occupation")]
        public string OccupationName { get; set; }

        [Display(Name = "Occupation")]
        public List<Occupation> OccupationList { get; set; }

        [Required(ErrorMessage = "Please select your Occupation.")]
        public int? SelectedOccupationId { get; set; }

        [Display(Name = "Do you want to be a Volunteer?")]
        public bool isSelected { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public string HashPassword { get; set; }

        [Required]
        [CompareAttribute("Password", ErrorMessage = "This Password is not same as the above.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }

        public string ImageFileName { get; set; }

        [DataType(DataType.ImageUrl)]
        public string ProfilePhotoPath { get; set; }

        public string RoleName { get; set; }
        public int? roleId { get; set; }

        public List<UserDetails> UsersList { get; set; }

    }
}