﻿
namespace BackToVillage.ViewModels
{
    public class StateVm
    {
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int SelectedStateId { get; set; }
    }
}