﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BackToVillage.Startup))]
namespace BackToVillage
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
