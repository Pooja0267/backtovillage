﻿
namespace BackToVillage.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string DOB { get; set; }
        public string Phone { get; set; }
        public string AltPhone { get; set; }
        public string Address { get; set; }

        public string Gender { get; set; }

        public int CityId { get; set; }
        public string CityName { get; set; }

        public int StateId { get; set; }
        public string StateName { get; set; }

        public string OccupationId { get; set; }
        public string OccupationName { get; set; }
        public bool isSelected { get; set; }


        public string Password { get; set; }

        public string HashPassword { get; set; }

        public int RoleId { get; set; }

        public string ImageFileName { get; set; }
        public string ImagePath { get; set; }
    }
}