﻿using System.Collections.Generic;

namespace BackToVillage.Models
{
    public class AddWork
    {
        public string Title { get; set; }
        public string Aim { get; set; }
        public int DomainId { get; set; }
        public string DomainName { get; set; }
        public float RequiredFund { get; set; }
        public int ProgressId { get; set; }

        public int CityId { get; set; }
        public string CityName { get; set; }

        public int StateId { get; set; }
        public string StateName { get; set; }

        public List<City> CityList { get; set; }
        public int SelectedCityId { get; set; }

        public List<State> StateList { get; set; }
        public int SelectedStateId { get; set; }

        public int Userid { get; set; }


    }
}